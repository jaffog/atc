<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>ATC</title>

		<link rel="stylesheet" href="styles/main.css">
	</head>
	<body>
		<div id="stage"></div>
		<script src="app/ATC.js"></script>
	</body>
</html>