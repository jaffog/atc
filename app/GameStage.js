(function( window ) {

"use strict";

function GameStage() {
}

GameStage.prototype.addElement = function( element ) {
	if ( !( element instanceof GameElement ) ) {
		throw new Error( "Invalid argument: instance of GameElement expected." );
	}
	
	this.elements.push( element );
};

window.GameStage = GameStage;

}( window )),