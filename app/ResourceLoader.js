(function( window ) {

"use strict";

function ResourceLoader( url ) {
	this.status = ResourceLoader.STATUS_IDLE;

	this.url = null;

	this.callbacks = {};
	this.callbacks[ ResourceLoader.STATUS_LOADING ] = [];
	this.callbacks[ ResourceLoader.STATUS_COMPLETE ] = [];

	if ( url !== undefined ) {
		this.setUrl( url );
	}
}

ResourceLoader.STATUS_IDLE = "idle";
ResourceLoader.STATUS_LOADING = "loading";
ResourceLoader.STATUS_COMPLETE = "complete";

ResourceLoader.prototype.setUrl = function( url ) {
	if ( typeof( url ) !== "string" ) {
		throw new Error( "Invalid argument: string expected." );
	}

	this.url = url;
};

ResourceLoader.prototype.getUrl = function() {
	return this.url;
};

ResourceLoader.prototype.setStatus = function( status ) {
	switch ( status ) {
		case ResourceLoader.STATUS_IDLE:
			if ( this.status !== ResourceLoader.STATUS_IDLE ) {
				throw new Error( "Cannot reset the status to idle." );
			}
			break;

		case ResourceLoader.STATUS_LOADING:
			if ( this.status === ResourceLoader.STATUS_IDLE ) {
				this.status = ResourceLoader.STATUS_LOADING;
				this.executeCallbacks();
			} else {
				throw new Error( "Can only set the status to loading if it was previously idle." );
			}
			break;

		case ResourceLoader.STATUS_COMPLETE:
			if ( this.status === ResourceLoader.STATUS_LOADING ) {
				this.status = ResourceLoader.STATUS_COMPLETE;
				this.executeCallbacks();
			} else {
				throw new Error( "Can only set the status to complete if it was previously loading." );
			}
			break;

		default:
			throw new Error( "Invalid argument: unknown status specified." );
	}
};

ResourceLoader.prototype.addCallback = function( status, callback ) {
	if ( !( status in this.callbacks ) ) {
		throw new Error( "Invalid argument: callbacks not supported on this status." );
	}

	if ( typeof( callback ) !== "function" ) {
		throw new Error( "Invalid argument: function expected." );
	}

	this.callbacks[ status ].push( callback );
};

ResourceLoader.prototype.executeCallbacks = function() {
	var n, callbacks,
		i = 0;

	if ( this.status in this.callbacks ) {
		callbacks = this.callbacks[ this.status ];

		for ( n = callbacks.length; i < n; i++ ) {
			callbacks[ i ]();
		}
	}
};

ResourceLoader.prototype.load = function( onSuccessCallback ) {
	if ( this.status !== ResourceLoader.STATUS_IDLE ) {
		throw new Error( "Loading failed: loader already active." );
	}
};

window.ResourceLoader = ResourceLoader;

}( window ));