(function( $ ) {

"use strict";

function ATC( stageId ) {

	this.stage = null;
	
	this.initStage();
	this.loadResources();

}

ATC.prototype.initStage = function() {
	if ( this.stage !== null ) {
		return;
	}
	
	this.stage = new GameStage();
};


}( jQuery ));